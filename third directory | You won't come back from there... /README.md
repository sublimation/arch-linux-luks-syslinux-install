Okay, now... it is time! <br>
<img src="https://gitlab.com/habben/arch-linux-luks-syslinux-install/raw/master/images/time.png" />
<br><br>
So you tought installing syslinux was hard? You stupid! It's even easier than grub! (At least for Arch Linux, I blame you if you are on Gentoo haha)
<br>
- <code>syslinux-install_update -iam</code>
- <code>vim /boot/syslinux/syslinux.cfg</code>

<br>Okay so we have a base, now you have to edit just like me the APPEND line : <br>

<img src="https://gitlab.com/habben/arch-linux-luks-syslinux-install/raw/master/images/gitlab7.png" />

<br>***IF YOU INSTALL WITH SWAP, PLEASE REFER TO THIS :*** https://gitlab.com/habben/arch-linux-luks-syslinux-install/blob/master/syslinux.cfg
<br>
Now, It's over :)
<br> Do a <code>exit</code> then <code>umount -R /mnt</code> and finally : <code> reboot </code>
