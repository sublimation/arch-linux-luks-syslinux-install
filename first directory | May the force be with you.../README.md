# Hello friends! Let's go for the very first step (the hardest one) 

**STOP** <br>


First, identify the disk you want to use : <br> <img src="https://gitlab.com/habben/arch-linux-luks-syslinux-install/raw/master/images/gitlab1.png" /> <br>

So here the disk I want to use is /dev/sda. So now I go for a cfdisk time : <br> <img src="https://gitlab.com/habben/arch-linux-luks-syslinux-install/raw/master/images/gitlab2.png" /> <br>

<br><br>

/dev/sda1 --> 200M --> Boot partition (no luks here)
<br>
/dev/sda2 --> 100%FREE --> The encrypted installation (lvm + luks)
<br>
**Now, simply format /dev/sda1 to ext2. <br> Then go for a cryptsetup to format /dev/sda2 and open it. <br><br> Once done, we create the mapper for the luks partition
, then we create the volumes :** <br>
<img src="https://gitlab.com/habben/arch-linux-luks-syslinux-install/raw/master/images/gitlab3.png" /> <br>

_Time to explain!_ 

<code>#mkfs.ext2 /dev/sda1</code> --> Format /dev/sda1 (Boot partition) Nothing unusual right? <br>
<code>#cryptsetup -c serpent-xts-plain64 --hash sha512 --key-size 512 luksFormat /dev/sda2</code> <br><br>
Hard time here ;) 
<code>-c serpent-xts-plain64</code> --> One of the most secure algo for a good crypto. Always trust the snake! <br>
<code> --hash sha512 --key-size 512</code> We want our computer to be the most secure right? So let's use a hash with a good quality and a strong key size (Not like d*cks, longer the key is, weaker it is) <br>
<br>
Now, just format our partitions. For instance : <br>
- <code>mkfs.ext4 /dev/mapper/vg0-root</code> (/ partition)
- <code>mkfs.ext4 /dev/mapper/vg0-home</code> (/home partition)
- <code>mkfs.ext4 /dev/mapper/vg0-var</code> (/var partition) **Attention! In my guide I made a var partition but you may want a swap one! So here it is** :
- First, do a <code>lvcreate -L "RAM SIZE" vg0 --name swap</code> then <code>mkswap /dev/mapper/vg0-swap</code> and finally : <code>swapon /dev/mapper/vg0-swap</code>
- Do not forget the <code>mkfs.ext2 /dev/sda1<code>

<br><br>
<img src="https://gitlab.com/habben/arch-linux-luks-syslinux-install/raw/master/images/git.png" />
<br><br>
Okay here we mount all the partitions.
- <code>mount /dev/mapper/vg0-root /mnt</code>
- <code>mkdir /mnt/{boot,var,home}</code>
- <code>mount /dev/sda1 /mnt/boot</code>
- <code>mount /dev/mapper/vg0-var /mnt/var</code>
- <code>mount /dev/mapper/vg0-home /mnt/home</code>

<br>
Now : <code>pacstrap -i /mnt base base-devel openssh vim git syslinux</code>



