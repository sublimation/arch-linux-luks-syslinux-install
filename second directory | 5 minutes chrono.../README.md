First, let's do a <code>genfstab -U -p /mnt >> /mnt/etc/fstab</code>
<br>
Now, we are good. Okay, let's chroot inside our box!
<br>
<code>arch-chroot /mnt</code>
<br>
Follow those steps :
<br>
- <code>echo FckingTux > /etc/hostname</code>
- <code>echo '127.0.1.1 FckingTux.localdomain FckingTux' >> /etc/hosts</code>
- <code>ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime</code>
- <code>locale-gen</code>
- <code>echo LANG="fr_FR.UTF-8" > /etc/locale.conf</code>
- <code>export LANG=fr_FR.UTF-8</code>
- <code>echo KEYMAP=fr > /etc/vconsole.conf</code>


<br>Yep, you just follow your favorite wiki. 
<br>
Then, let's dig in the hard stuff! (No, I'm kidding.)
<br>
<code>vim /etc/mkinitcpio.conf</code>
<br>Then just do like me, you have to add "ext4" into MODULES() and "encrypt" + "lvm2" into HOOKS **before** "filesystem"
<br><br>
<img src="https://gitlab.com/habben/arch-linux-luks-syslinux-install/raw/master/images/gitlab6.png" />
<br><br>
And... **over** you can go for the last part of this tutorial :)